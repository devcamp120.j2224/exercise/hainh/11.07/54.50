package com.example.demo;

import java.util.ArrayList;
import java.util.Iterator;

public class Main {
    public static void main(String[] args) {

        // tạo 1 arraylist vs object là countries
        ArrayList<Country> countries = new ArrayList<Country>();
        // tạo class Country truyền vào 2 tham số 
		Country Country = new Country("VietNam", "Vn");
        // dùng hàm set bên class country để set lại giá trị cho object Country
		Country.setCountryName("Việt Nam");
		Country.setCountryCode("VN");
        // add object Country vào mảng arraylist countries
		countries.add(Country);
        /* 
         * Iterator trong Java là một interface được sử dụng để thay thế Enumerations trong Java Collection Framework. Bạn có thể sử dụng interator để:

                Duyệt các phần tử từ đầu đến cuối của một collection.
                Iterator cho phép xóa phần tử khi lặp một collection.

                hasNext(): có phần tử tiếp theo hay không

                next(): lấy phần tử tiếp theo

                remove(): loại bò phần tử cuối cùng
         */
		 for (Iterator iterator = countries.iterator(); iterator.hasNext();) {
            // ép kiểu 
			Country Country1 = (Country) iterator.next();

            // nếu giá trị "VN" bằng (equals) Country1 là giá trị tiếp theo dc chấm với getCountryCode ( là lấy giá trị )
			if("VN".equals(Country1.getCountryCode())) {
				System.out.println("vùng là : " + Country.getCountryCode());
			}
		}
		
		ArrayList<Region> regions = new ArrayList<Region>();
		Region Region = new Region("Ha Nội", "HN");
		Region Region1 = new Region("Hồ Chí Minh", "HCM");
		regions.add(Region);
		regions.add(Region1);
        // setRegion này là bên class Country 
        // dùng class Country tạo ở trên set region bỏ vào đó là object regions dc tạo từ arraylist
		Country.setRegion(regions);
		
        System.out.println("các vùng của :  " + Country.getCountryCode() + " là : " + countries );
    }
}
